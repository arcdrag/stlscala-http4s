package arcdrag.stlscalahttp4s

import arcdrag.stlscalahttp4s.model.{Crime, LatLonCoord}


trait CrimeFinder {
  def getNearestCrime(point: LatLonCoord): Crime
}

/**
  * A CrimeFinder implementation that receives all crimes as input upon instantiation and thus can query in memory directly.
  * @param crimes The crimes that occurred in the St. Louis area.
  */
class InMemoryCrimeFinder(crimes: Seq[Crime]) extends CrimeFinder {

  // This realistically should query an RTree or something, but we're keeping it dumb because I really didn't want to
  // introduce geotools.  Maybe some other month I'll do a lightning talk on load testing and we can see how much an RTree
  // improves performance here :)
  def getNearestCrime(point: LatLonCoord): Crime = {
    crimes.minBy(c => c.coords.distSq(point))
  }
}

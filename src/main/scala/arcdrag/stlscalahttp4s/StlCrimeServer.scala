package arcdrag.stlscalahttp4s

import cats.effect.{Effect, IO}
import fs2.StreamApp
import org.http4s.server.blaze.BlazeBuilder

import scala.concurrent.ExecutionContext
import scala.util.Properties

object StlCrimeServer extends StreamApp[IO] with ServerStartup[IO] {
  import scala.concurrent.ExecutionContext.Implicits.global

  implicit val crimeService: StlCrimeService[IO] = startUp

  def stream(args: List[String], requestShutdown: IO[Unit]) = ServerStream.stream[IO]
}

object ServerStream {

  // This is a heroku property.  It changes each time the server starts.  Assuming you don't have this environment variable
  // set locally you'll always get 8080 in dev mode.
  lazy val port: Int = Properties.envOrElse("PORT", "8080").toInt

  def stream[F[_]: Effect](implicit ec: ExecutionContext, stlCrimeService: StlCrimeService[F]) =
    BlazeBuilder[F]
      .bindHttp(port, "0.0.0.0")
      .mountService(stlCrimeService.service, "/")
      .serve
}

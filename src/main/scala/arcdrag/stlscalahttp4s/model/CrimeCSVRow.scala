package arcdrag.stlscalahttp4s.model

import java.time.LocalDate


case class CrimeCSVRow(ucrCount: Int,
                       complaintYear: Int,
                       complaintNum: Int,
                       ucrOffense: String,
                       ucrCrimeCode: Int,
                       typ: String,
                       month: String,
                       year: Int,
                       callReceivedAt: String,
                       occurredAt: LocalDate,
                       dayOfWeekOccurred: String,
                       zone: Int,
                       address: String,
                       precinct: String,
                       premise: String,
                       reportingJurisdiction: String,
                       forJurisdiction: String,
                       xCoordOpt: Option[Double],
                       yCoordOpt: Option[Double],
                       objectId: Int) {
  def toCrime: Option[Crime] = {
    for {
      lon <- xCoordOpt
      lat <- yCoordOpt
    } yield
      Crime(LatLonCoord(lat, lon),
        address,
        precinct,
        dayOfWeekOccurred,
        occurredAt.toString,
        ucrOffense, ucrCrimeCode)
  }
}

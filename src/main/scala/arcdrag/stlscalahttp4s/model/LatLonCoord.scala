package arcdrag.stlscalahttp4s.model

case class LatLonCoord(lat: Double, lon: Double) {
  def distSq(other: LatLonCoord): Double = {
    val latDiff = lat - other.lat
    val lonDiff = lon - other.lon
    (latDiff * latDiff) + (lonDiff * lonDiff)
  }
}

object LatLonCoord {
  // semiautomatic derivation required to convert this guy into json in the response.
  import io.circe._, io.circe.generic.semiauto._
  lazy implicit val crimeDecoder: Decoder[LatLonCoord] = deriveDecoder[LatLonCoord]
  lazy implicit val crimeEncoder: Encoder[LatLonCoord] = deriveEncoder[LatLonCoord]
}


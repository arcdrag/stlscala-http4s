package arcdrag.stlscalahttp4s.model

case class Crime(coords: LatLonCoord,
                 address: String,
                 precinct: String,
                 dayOfWeekOccurred: String,
                 occurredAt: String,
                 ucrOffense: String,
                 ucrCrimeCode: Int)

object Crime {
  // semiautomatic derivation required to convert this guy into json in the response.
  // Do note that LatLonCoord needs a similar thing or this would blow up with an unintelligible error message.
  import io.circe._, io.circe.generic.semiauto._
  lazy implicit val crimeDecoder: Decoder[Crime] = deriveDecoder[Crime]
  lazy implicit val crimeEncoder: Encoder[Crime] = deriveEncoder[Crime]
}

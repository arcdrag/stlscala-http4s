package arcdrag.stlscalahttp4s

import arcdrag.stlscalahttp4s.model.LatLonCoord
import cats.effect.Effect
import io.circe.syntax._
import org.http4s.HttpService
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl

class StlCrimeService[F[_]: Effect](crimeFinder: CrimeFinder) extends Http4sDsl[F] {

  object LatitudeQueryParamMatcher
    extends QueryParamDecoderMatcher[Double]("lat")

  object LongitudeQueryParamMatcher
    extends QueryParamDecoderMatcher[Double]("lon")

  /**
    * Our lone endpoint here is a GET with 2 query params (lat & lon).  It finds the nearest crime from the St. Louis
    * crime "database" to that point and returns it.
    */
  val service: HttpService[F] = {
    HttpService[F] {
      case GET -> Root / "stlcrime" :?
        LatitudeQueryParamMatcher(latitude) +&
          LongitudeQueryParamMatcher(longitude) =>
        Ok(crimeFinder.getNearestCrime(LatLonCoord(latitude, longitude)).asJson)
    }
  }
}

package arcdrag.stlscalahttp4s

import cats.effect.Effect
import fs2.StreamApp

import scala.util.{Failure, Success, Try}

trait ServerStartup[T[_]] {
  // self type prevents this trait from being mixed in to anything but a StreamApp.  This is kinda-sorta hacky, but it
  // works effectively in preventing it from being called anywhere but server startup time.
  this: StreamApp[T] =>

  // Builds the object graph necessary to serve data.  This would do things like read in config, read reference data from
  // an external file server, and other actions that you want to only happen once when the server first starts up.
  def startUp[F[_] : Effect] : StlCrimeService[F] = {
    println("Beginning Server Startup")
    val crimes = Try {
      println("Reading file.")
      val crimeUri = getClass.getResource("/stl2018crime.csv")
      val reader = new CrimeCSVReader(crimeUri)
      reader.getCrime.unsafeRunSync()
    } match {
      case Success(s) => s
      case Failure(t) =>
        println(t)
        throw t
    }

    println(s"Successfully read ${crimes.size} crimes from file.")
    val dao = new InMemoryCrimeFinder(crimes)
    new StlCrimeService[F](dao)

  }
}

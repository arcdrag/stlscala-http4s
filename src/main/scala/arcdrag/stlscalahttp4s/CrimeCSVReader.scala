package arcdrag.stlscalahttp4s

import java.net.URL
import java.time.LocalDate
import java.time.format.DateTimeFormatter

import arcdrag.stlscalahttp4s.model.{Crime, CrimeCSVRow}
import cats.effect.IO
import kantan.csv._
import kantan.csv.ops._
import kantan.csv.generic._
import kantan.csv.java8._


class CrimeCSVReader(url: URL) {

  //Since St. Louis county government are savages, they don't use ISO8601 dates and we need to provide a specialized
  //formatter so kantan.csv can figure out how to format them.
  private val format: DateTimeFormatter =
    DateTimeFormatter.ofPattern("M/d/yyyy")
  implicit val decoder: CellDecoder[LocalDate] = localDateDecoder(format)

  /**
    * Reads in our CSV file containing St. Louis Crime data and converts to the format we want to work with
    * Throws away rows that are malformed (namely those that don't have coordinates).
    * @return an IO that when executed contains all of the crimes in our St. Louis crimes "database".
    */
  def getCrime: IO[Seq[Crime]] = IO {

    // This is a pattern I tend to follow but I'm not settled on if it is great or not.  I tend to parse CSV files
    // directly into a product class that almost identically matches the CSV format.  Kantan has levers you can pull
    // to more directly convert to the format you want, but first converting to a format that matches the file and then
    // converting from that format to the one you actually want requires no magic and has less risk of losing an hour
    // searching for a missing implicit.
    val reader = url.asCsvReader[CrimeCSVRow](rfc.withHeader)
    reader
      .collect {
        case Right(t) => t.toCrime
      }
      .flatten
      .toSeq
  }

}

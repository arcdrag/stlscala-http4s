package arcdrag.stlscalahttp4s

import arcdrag.stlscalahttp4s.model.{Crime, LatLonCoord}
import cats.effect.IO
import org.http4s._
import org.http4s.implicits._
import org.specs2.matcher.MatchResult

class CrimeServiceSpec extends org.specs2.mutable.Specification {

  "CrimeService" >> {
    "return 200" >> {
      uriReturns200()
    }
    "return fake crime from stub crimeFinder" >> {
      uriReturnsStubCrime()
    }
  }

  private val stubCrimeFinder = new CrimeFinder {
    override def getNearestCrime(point: LatLonCoord): Crime = Crime(LatLonCoord(0d, 0d), "address", "precinct", "dow", "occurredAt", "ucrOffense", 1)
  }

  private[this] val retStub: Response[IO] = {
    val getHW = Request[IO](Method.GET, Uri.uri("/stlcrime?lat=38.807021&lon=-90.37173"))
    new StlCrimeService[IO](stubCrimeFinder).service.orNotFound(getHW).unsafeRunSync()
  }

  private[this] def uriReturns200(): MatchResult[Status] =
    retStub.status must beEqualTo(Status.Ok)

  private[this] def uriReturnsStubCrime(): MatchResult[String] =
    retStub.as[String].unsafeRunSync() must beEqualTo("""{"coords":{"lat":0.0,"lon":0.0},"address":"address","precinct":"precinct","dayOfWeekOccurred":"dow","occurredAt":"occurredAt","ucrOffense":"ucrOffense","ucrCrimeCode":1}""")
}
